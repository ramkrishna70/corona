import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'; 
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    private static apiBaseUrl:string = ""; 
  	private headers: HttpHeaders;

	/**
	* Constructor method
	*
	* @Arguments:
	*	<http>: For handling http requests
	*	<userdata>: For checking network
	*
	*/
	constructor(private http: HttpClient ) {
		this.headers = new HttpHeaders({
			//'User-Agent': 'NodeJS XML-RPC Client',
			'Content-Type': 'application/json',
			'Accept': 'application/json',
			//'Accept-Charset': 'UTF8',
			//'Connection': 'Keep-Alive'
    	});
	}
 

    /**
    * Method to check weeather a given string is 
    * JSON or not
    *
    * @Arguments: 
    *   <str>: String
    *
    * @Returns: boolean
    */
    public isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    

    /**
    * Method to call the api
    *
    * @Arguments:
    *   <url>: partial url to be called
    *   <options>: Object containing the header, body etc.
    *
    * @Return: Prmose.
    */
    public callApi(url:string, options:any={}):Promise<any> {
        return new Promise((resolve, reject) => {
           /* if(this.connectivity.isOnline){
                this.userdata.pop_alert("Failed","Internet Connecetion could not be found.",["OK"]);
                reject("No Network Found!");
            }*/
            console.log(options);
            let fullUrl:string = ApiService.apiBaseUrl.replace(/\/$/,'')
                               + "/" 
                               + url.replace(/^\//,'');
            let request:any;
            let body:any = ""; 
            let responseObj:any = {}; 
 
            
            // If the headers provided in options is 
            // object then include them in header also.
            if (typeof(options.headers) == "object") {
                for (let key in options.headers) {
                    this.headers.append(key, options.headers[key]); 
                }
            }

            // If the given body is object, then convert
            // them into body.
            if (typeof(options.body) == "object") {
                body = JSON.stringify(options.body);
            }
            let requestMehod: string = (options.method) ? options.method.toLowerCase() : "get";
            switch(requestMehod) {
                case "get":
                    request = this.http.get(fullUrl, { headers: this.headers, observe: "body", responseType: "json" });
                    break;
                case "post":
                    request = this.http.post(fullUrl, body, { headers: this.headers, observe: "body", responseType: "json" });
                    break;
                case "put":
                    request = this.http.put(fullUrl, body, { headers: this.headers, observe: "body", responseType: "json" });
                    break;
                case "delete":
                    break;
            }
            request.subscribe(data => { 
                responseObj.status  = data.status;
                responseObj.isError = false;
                responseObj.headers = data.headers;
                responseObj.body    = (this.isJson(data.text())) ? data.json() : data.text();
                return resolve(responseObj);
            }, error => { 
                responseObj.status  = error.status;
                responseObj.isError = true;
                responseObj.headers = error.headers;
                responseObj.body    = (this.isJson(error.text())) ? error.json() : error.text();
                return reject(responseObj);
            }); 
        });
    } 
}