import { environment as env} from '@env';

export class URL {

	constructor() {}

	get_path(path:string = "affected") {  
		return env.protocol + env.domain + '/' + env.end_point + '/' + path + '.' + 'php';
	}

	get_path_with_param(path:string, param:string) {
		let url = this.get_path(path);
		return url + '?' + param;
	}
}